package com.example.seleniumResearch.testOne;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class JenkinsDemo {

    @Test
    public void testgooglrsearch() {
//        System.setProperty("webdriver.gecko.driver", "C:\\Users\\dushman.d\\Desktop\\Life\\Jenkins\\SpringProject\\seleniumResearch\\seleniumresearch\\src\\test\\resources\\lib\\gickodriver\\geckodriver.exe");
//        System.setProperty("webdriver.firefox.bin", "C:\\Program Files\\Mozilla Firefox\\firefox.exe");
//        WebDriver webDriver = new FirefoxDriver();
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\dushman.d\\Desktop\\Life\\Jenkins\\SpringProject\\seleniumResearch\\seleniumresearch\\src\\test\\resources\\lib\\chromedriver\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
//it will open the goggle page
        webDriver.get("http://google.in");
//we expect the title “Google “ should be present
        String Expectedtitle = "Google";
//it will fetch the actual title
        String Actualtitle = webDriver.getTitle();
        System.out.println("Before Assetion " + Expectedtitle + Actualtitle);
        Assert.assertEquals(Actualtitle, Expectedtitle);
//it will compare actual title and expected title
//print out the result
        System.out.println("After Assertion " + Expectedtitle + Actualtitle + " Title matched ");
    }
}


