package com.example.seleniumResearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeleniumResearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeleniumResearchApplication.class, args);
	}

}
