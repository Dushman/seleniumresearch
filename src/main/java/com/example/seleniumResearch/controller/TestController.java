package com.example.seleniumResearch.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, value = "/api/testCICD")
    public String getAllBranches() {
        return "well come to Selenium CICD";
    }

}
